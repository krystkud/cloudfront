module "waf_global" {
  providers = {
    aws = aws.nvirginia
  }
  source          = "git::ssh://git@bitbucket.oberthur.com/cat/terraform-modules-aws.git//modules/waf/global?ref=develop"
  globals         = module.common.globals
  LOGS_BUCKET_ARN = module.frontend_logs_bucket.s3_bucket_arn
  REDACTED_FIELDS = [{
    field_to_match = [{
      type = "URI"
    }]
  }]
}
