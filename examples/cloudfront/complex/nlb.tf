module "nlb" {
  source                = "git::https://bitbucket.org/digitallabsbuild/ingress.git//modules/nlb?ref=master"
  globals               = module.common.globals
  networking            = module.networking.common
  PUBLIC_SUBNET_IDS     = module.networking.public_subnet_ids
  ENABLE_HTTPS_LISTENER = false
  INTERNAL              = false
}
