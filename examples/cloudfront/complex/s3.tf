resource "random_pet" "pet" {
}

module "bucket_1" {
  source             = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/bucket?ref=master"
  globals            = module.common.globals
  BUCKET_NAME        = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-${random_pet.pet.id}"
  SSE_ALGORITHM      = "AES256"
  ACCESS_LOGS_BUCKET = module.frontend_logs_bucket.s3_bucket_name
  FORCE_DESTROY      = true
}

module "bucket_2" {
  source        = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/bucket?ref=master"
  globals       = module.common.globals
  BUCKET_NAME   = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-${random_pet.pet.id}-2"
  FORCE_DESTROY = true
}

module "bucket_3" {
  source        = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/bucket?ref=master"
  globals       = module.common.globals
  BUCKET_NAME   = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-${random_pet.pet.id}-3"
  FORCE_DESTROY = true
}

module "frontend_logs_bucket" {
  source          = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/logs"
  globals         = module.common.globals
  EXPIRATION_DAYS = "90"
  SSE_ALGORITHM   = "AES256"
  FORCE_DESTROY   = true
  BUCKET_NAME     = random_pet.pet.id
}

resource "aws_s3_bucket_object" "index" {
  key                    = "index.html"
  bucket                 = module.bucket_1.s3_bucket_name
  source                 = "./website/index.html"
  etag                   = filemd5("./website/index.html")
  server_side_encryption = "AES256"
}

resource "aws_s3_bucket_object" "error" {
  key                    = "404.html"
  bucket                 = module.bucket_1.s3_bucket_name
  source                 = "./website/404.html"
  etag                   = filemd5("./website/404.html")
  server_side_encryption = "AES256"
}

resource "aws_s3_bucket_object" "forbidden" {
  key                    = "403.html"
  bucket                 = module.bucket_1.s3_bucket_name
  source                 = "./website/403.html"
  etag                   = filemd5("./website/403.html")
  server_side_encryption = "AES256"
}

resource "aws_s3_bucket_object" "index_2" {
  key                    = "front2/index.html"
  bucket                 = module.bucket_2.s3_bucket_name
  source                 = "./website/page.html"
  etag                   = filemd5("./website/page.html")
  server_side_encryption = "AES256"
}

resource "aws_s3_bucket_object" "index_3" {
  key                    = "front3/index.html"
  bucket                 = module.bucket_3.s3_bucket_name
  source                 = "./website/web.html"
  etag                   = filemd5("./website/web.html")
  server_side_encryption = "AES256"
}
