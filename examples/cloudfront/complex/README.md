### This example creates a public CloudFront distribution serving content from a private encrypted S3 bucket. OAI is set, so only CF is allowed to serve the s3 content. WAF is enabled for the distribution (logs only). Another S3 bucket is created for logs storing. This is using existing cat-test.idemia.io zone for simplicity. Check the outputs for the website link. Try the 404 page too. A lambda to redirect non-index.html paths is in place: https://grrr.tech/posts/cloudfront-www-redirect/

> content of the `variables.tf` file:

```
data "aws_caller_identity" "current" {}

variable "AWS_REGION" {
  default     = "us-east-1"
  description = "Provide aws region"
}

variable "ENVIRONMENT" {
  default     = "test"
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
}

variable "PROJECT" {
  description = "Provide name of the project (max 20 characters)"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "DNS_ZONE_NAME" {
  description = "Provide dns zone name"
}

variable "NAME" {
  description = "Name for Athena tables and DBs"
  default     = "database"
}

```

> content of the `terraform.tfvars` file:

```
##
# Provide aws region
##
AWS_REGION = "eu-central-1"

##
# Provide name of the project (max 20 characters)
##
PROJECT = "cloudfront"

##
# Provide type of the environment, ex dev|test|stg|prod (max 4 characters)
##
ENVIRONMENT = "frontend"

##
# Provide aws account name
##
ACCOUNT_NAME = "teribu"

##
# Provide dns zone name
##
DNS_ZONE_NAME = "cat-test.idemia.io"

```
