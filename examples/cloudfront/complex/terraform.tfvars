##
# Provide aws region
##
AWS_REGION = "eu-central-1"

##
# Provide name of the project (max 20 characters)
##
PROJECT = "cloudfront"

##
# Provide type of the environment, ex dev|test|stg|prod (max 4 characters)
##
ENVIRONMENT = "frontend"

##
# Provide aws account name
##
ACCOUNT_NAME = "teribu"

##
# Provide dns zone name
##
DNS_ZONE_NAME = "cat-test.idemia.io"

##
# Provide VPC CIDR block
##
VPC_CIDR_BLOCK          = "10.10.0.0/16"

##
# Provide public subnets' blocks
##
PUBLIC_CIDR_BLOCKS      = ["10.10.0.0/24", "10.10.1.0/24"]

##
# Provide application subnets' blocks
##
APPLICATION_CIDR_BLOCKS = ["10.10.10.0/24", "10.10.11.0/24"]
