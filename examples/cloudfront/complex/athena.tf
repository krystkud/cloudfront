module "athena_bucket" {
  source             = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/bucket?ref=master"
  globals            = module.common.globals
  BUCKET_NAME        = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-athena-${random_pet.pet.id}"
  SSE_ALGORITHM      = "AES256"
  ACCESS_LOGS_BUCKET = module.frontend_logs_bucket.s3_bucket_name
  FORCE_DESTROY      = true
}

module "athena" {
  source             = "git::https://bitbucket.org/digitallabsbuild/athena.git//modules?ref=master"
  globals            = module.common.globals
  NAME               = var.NAME
  DESTINATION_BUCKET = "${module.athena_bucket.s3_bucket_name}/queries"
  QUERIES = {
    create_waf_logs = {
      name        = "create_waf_logs"
      description = "Create a table to query WAF logs"
      query       = data.template_file.create_waf_logs.rendered
    },
    create_s3_access_logs = {
      name        = "create_s3_access_logs"
      description = "Create a table to query S3 Access logs"
      query       = data.template_file.create_s3_access_logs.rendered
    },
    create_cloudfront_logs = {
      name        = "create_cloudfront_logs"
      description = "Create a table to query CloudFront Access logs"
      query       = data.template_file.create_cloudfront_logs.rendered
    }
  }
}

data "template_file" "create_waf_logs" {
  template = file("./templates/create_waf_logs.sql.tpl")

  vars = {
    bucket = module.frontend_logs_bucket.s3_bucket_name
  }
}

data "template_file" "create_s3_access_logs" {
  template = file("${path.module}/templates/create_s3_access_logs.sql.tpl")

  vars = {
    bucket = module.frontend_logs_bucket.s3_bucket_name
  }
}

data "template_file" "create_cloudfront_logs" {
  template = file("./templates/create_cloudfront_logs.sql.tpl")

  vars = {
    bucket = module.frontend_logs_bucket.s3_bucket_name
  }
}
