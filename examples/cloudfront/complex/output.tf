
output "CloudFront" {
  value = <<OUTPUT
(ง '̀͜ '́ )ง FRONTEND

ID           :: ${module.frontend.cf_id}

ARN          :: ${module.frontend.cf_arn}

Domain Name  :: ${module.frontend.cf_domain_name}

Bucket 1 Name  :: ${module.bucket_1.s3_bucket_name}

Bucket 1 ARN   :: ${module.bucket_1.s3_bucket_arn}

Bucket 1 Domain Name :: ${module.bucket_1.bucket_domain_name}

Bucket 2 Name  :: ${module.bucket_2.s3_bucket_name}

Bucket 2 ARN   :: ${module.bucket_2.s3_bucket_arn}

Bucket 2 Domain Name :: ${module.bucket_2.bucket_domain_name}

Bucket 3 Name  :: ${module.bucket_3.s3_bucket_name}

Bucket 3 ARN   :: ${module.bucket_3.s3_bucket_arn}

Bucket 3 Domain Name :: ${module.bucket_3.bucket_domain_name}

Logs Bucket  :: ${module.frontend_logs_bucket.s3_bucket_name}

Logs Bucket Domain Name :: ${module.frontend_logs_bucket.s3_bucket_domain_name}

Lambda Function Name :: ${module.lambda.function_name}

Lambda ARN :: ${module.lambda.lambda_arn}

Lambda 2 Function Name :: ${module.lambda2.function_name}

Lambda 2 ARN :: ${module.lambda2.lambda_arn}

Lambda 3 Function Name :: ${module.lambda3.function_name}

Lambda 3 ARN :: ${module.lambda3.lambda_arn}

CHECK to see if all is working
curl https://website.${var.DNS_ZONE_NAME}
curl https://website.${var.DNS_ZONE_NAME}/front2/index.html
curl https://website.${var.DNS_ZONE_NAME}/front3/index.html

OUTPUT
}

output "WAF" {
  value = <<OUTPUT
(~‾▿‾)~ GLOBAL

Web ACL ID    :: ${module.waf_global.web_acl_id}

Web ACL Name  :: ${module.waf_global.web_acl_name}

###################################################################################

OUTPUT
}

output "ATHENA" {
  value = <<OUTPUT
(ง '̀͜ '́ )ง ATHENA

Athena Bucket     :: ${module.athena_bucket.s3_bucket_name}

ID                :: ${module.athena.athena_ID}

###################################################################################

OUTPUT
}

output "queries" {
  value = module.athena.queries
}
