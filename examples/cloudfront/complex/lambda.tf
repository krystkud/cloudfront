module "lambda" {
  providers = {
    aws = aws.nvirginia
  }
  source           = "git::https://bitbucket.org/digitallabsbuild/lambda.git//modules/lambda?ref=master"
  globals          = module.common.globals
  NAME             = "redirects"
  FILENAME         = "index.js"
  LAMBDA_PATH      = "./lambdas"
  HANDLER          = "index.handler"
  RUNTIME          = "nodejs10.x"
  TRUSTED_SERVICES = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
  IAM_PERMISSIONS = [{
    IAM_RESOURCE = ["*"]
    IAM_ACTIONS  = ["logs:PutLogEvents", "logs:CreateLogStream", "logs:CreateLogGroup"]
  }]
  STATEMENT_IDS = [
    "AllowExecutionFromLambda",
    "AllowExecutionFromEdge"
  ]
  PRINCIPALS = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
}

module "lambda2" {
  providers = {
    aws = aws.nvirginia
  }
  source           = "git::https://bitbucket.org/digitallabsbuild/lambda.git//modules/lambda?ref=master"
  globals          = module.common.globals
  NAME             = "cf-response"
  FILENAME         = "index.js"
  LAMBDA_PATH      = "./lambdas"
  HANDLER          = "index.handler"
  RUNTIME          = "nodejs10.x"
  TRUSTED_SERVICES = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
  IAM_PERMISSIONS = [{
    IAM_RESOURCE = ["*"]
    IAM_ACTIONS  = ["logs:PutLogEvents", "logs:CreateLogStream", "logs:CreateLogGroup"]
  }]
  STATEMENT_IDS = [
    "AllowExecutionFromLambda",
    "AllowExecutionFromEdge"
  ]
  PRINCIPALS = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
}

module "lambda3" {
  providers = {
    aws = aws.nvirginia
  }
  source           = "git::https://bitbucket.org/digitallabsbuild/lambda.git//modules/lambda?ref=master"
  globals          = module.common.globals
  NAME             = "cf-request"
  FILENAME         = "index.js"
  LAMBDA_PATH      = "./lambdas"
  HANDLER          = "index.handler"
  RUNTIME          = "nodejs10.x"
  TRUSTED_SERVICES = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
  IAM_PERMISSIONS = [{
    IAM_RESOURCE = ["*"]
    IAM_ACTIONS  = ["logs:PutLogEvents", "logs:CreateLogStream", "logs:CreateLogGroup"]
  }]
  STATEMENT_IDS = [
    "AllowExecutionFromLambda",
    "AllowExecutionFromEdge"
  ]
  PRINCIPALS = ["lambda.amazonaws.com", "edgelambda.amazonaws.com"]
}
