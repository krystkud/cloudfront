provider "aws" {
  region = var.AWS_REGION
}

provider "aws" {
  alias  = "nvirginia"
  region = "us-east-1"
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=1.0.0"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = var.ENVIRONMENT
  PROJECT      = var.PROJECT
}

module "wildcard_cert" {
  providers = {
    aws = aws.nvirginia
  }
  source        = "git::https://bitbucket.org/digitallabsbuild/dns.git//modules/certificates?ref=master"
  globals       = module.common.globals
  DNS_ZONE_NAME = var.DNS_ZONE_NAME
  ZONE_ID       = "ZYM997DTBGCL3"
  SUBDOMAIN     = "*"
}

resource "aws_route53_record" "cloudfront_domain_name" {
  zone_id = "ZYM997DTBGCL3"
  name    = "website"
  records = ["${module.frontend.cf_domain_name}"]
  type    = "CNAME"
  ttl     = "60"
}

module "frontend" {
  source                   = "../../../modules"
  globals                  = module.common.globals
  DOMAINS                  = ["website.${var.DNS_ZONE_NAME}"]
  CERTIFICATE_ARN          = tostring(module.wildcard_cert.certificate_arn)
  BUCKET_NAME              = module.bucket_1.s3_bucket_name
  ALLOWED_METHODS          = ["GET", "HEAD"]
  MAX_TTL                  = 31536000
  MINIMUM_PROTOCOL_VERSION = "TLSv1.2_2019"
  COMPRESS                 = false
  TRUSTED_SIGNERS          = ["self"]
  WEB_ACL_ID               = module.waf_global.web_acl_id
  ACCESS_LOGS_BUCKET       = module.frontend_logs_bucket.s3_bucket_domain_name
  CUSTOM_ERROR_RESPONSE = [{
    error_caching_min_ttl = 10
    error_code            = 404
    response_code         = 404
    response_page_path    = "/404.html"
    },
    {
      error_caching_min_ttl = 10
      error_code            = 403
      response_code         = 403
      response_page_path    = "/403.html"
  }]
  ORIGIN = [{
    bucket_name        = module.bucket_1.s3_bucket_name
    bucket_domain_name = module.bucket_1.bucket_domain_name
    s3_origin_config   = 0
    custom_header = [{
      custom_header_name  = "foo"
      custom_header_value = "bar"
    }]
    },
    {
      bucket_name        = module.bucket_2.s3_bucket_name
      bucket_domain_name = module.bucket_2.bucket_domain_name
      s3_origin_config   = 1
      custom_header = [{
        custom_header_name  = "foo"
        custom_header_value = "bar"
      }]
    },
    {
      bucket_name        = module.bucket_3.s3_bucket_name
      bucket_domain_name = module.bucket_3.bucket_domain_name
      s3_origin_config   = 2
      custom_header = [{
        custom_header_name  = "foo"
        custom_header_value = "bar"
      }]
  }]

  ELB_ORIGIN = [
    {
      ELB_DOMAIN_NAME = module.nlb.ingress_dns_name
      ELB_ZONE_ID     = module.nlb.ingress_zone_id
    }
  ]

  ORDERED_CACHE_BEHAVIOR = [{
    path_pattern           = "*.json"
    target_origin_id       = module.bucket_1.s3_bucket_name
    compress               = false
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    lambda_function_association = [{
      lambda_arn = "${module.lambda.lambda_arn}:${module.lambda.version}"
      event_type = "origin-request"
      },
      {
        lambda_arn = "${module.lambda2.lambda_arn}:${module.lambda2.version}"
        event_type = "origin-response"
    }]
    },
    {
      path_pattern           = "front2/*.html"
      target_origin_id       = module.bucket_2.s3_bucket_name
      compress               = true
      viewer_protocol_policy = "redirect-to-https"
      min_ttl                = 1
      default_ttl            = 10
      max_ttl                = 100
      lambda_function_association = [{
        lambda_arn = "${module.lambda2.lambda_arn}:${module.lambda2.version}"
        event_type = "origin-response"
      }]
    }
  ]
}
