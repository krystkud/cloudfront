AWS_REGION              = "us-east-1"
PROJECT                 = "cloudfront"
ENVIRONMENT             = "elb"
ACCOUNT_NAME            = "teribu"
VPC_CIDR_BLOCK          = "10.10.0.0/16"
PUBLIC_CIDR_BLOCKS      = ["10.10.0.0/24", "10.10.1.0/24"]
APPLICATION_CIDR_BLOCKS = ["10.10.10.0/24", "10.10.11.0/24"]
