output "CloudFront" {
  value = <<OUTPUT
(ง '̀͜ '́ )ง FRONTEND

ID           :: ${module.cloudfront.cf_id}

ARN          :: ${module.cloudfront.cf_arn}

Domain Name  :: ${module.cloudfront.cf_domain_name}

OUTPUT
}
