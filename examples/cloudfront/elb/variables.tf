variable "AWS_REGION" {
  default     = "us-east-1"
  description = "Provide aws region"
}

variable "ENVIRONMENT" {
  default     = "test"
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
}

variable "PROJECT" {
  description = "Provide name of the project (max 20 characters)"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "VPC_CIDR_BLOCK" {
  description = "Provide aws cidr block for whole vpc"
  default     = "10.1.0.0/24"
}

variable "PUBLIC_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of public cidr blocks per availability zones, ex azX = '10.1.1.0/24'"
  type        = list(string)
}

variable "APPLICATION_CIDR_BLOCKS" {
  default     = []
  description = "Provide list of application cidr blocks per availability zones, ex azX = '10.1.10.0/24'"
  type        = list(string)
}
