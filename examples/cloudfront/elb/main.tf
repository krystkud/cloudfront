provider "aws" {
  region = var.AWS_REGION
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=1.0.0"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = var.ENVIRONMENT
  PROJECT      = var.PROJECT
}

module "networking" {
  source                  = "git::https://bitbucket.org/digitallabsbuild/vpc.git//modules/networking?ref=master"
  globals                 = module.common.globals
  VPC_CIDR_BLOCK          = var.VPC_CIDR_BLOCK
  PUBLIC_CIDR_BLOCKS      = var.PUBLIC_CIDR_BLOCKS
  APPLICATION_CIDR_BLOCKS = var.APPLICATION_CIDR_BLOCKS
}

module "nlb" {
  source                = "git::https://bitbucket.org/digitallabsbuild/ingress.git//modules/nlb?ref=master"
  globals               = module.common.globals
  networking            = module.networking.common
  PUBLIC_SUBNET_IDS     = module.networking.public_subnet_ids
  ENABLE_HTTPS_LISTENER = false
  INTERNAL              = false
}

module "cloudfront" {
  source                 = "../../../modules/"
  globals                = module.common.globals
  ORIGIN_PROTOCOL_POLICY = "match-viewer"
  ELB_ORIGIN = [
    {
      ELB_DOMAIN_NAME = module.nlb.ingress_dns_name
      ELB_ZONE_ID     = module.nlb.ingress_zone_id
    }
  ]
}
