### This example creates a public CloudFront distribution serving content from a private encrypted S3 bucket. OAI is set, so only CF is allowed to serve the s3 content. WAF is enabled for the distribution (logs only). Another S3 bucket is created for logs storing. This is using existing cat-test.idemia.io zone for simplicity. Check the outputs for the website link. Try the 404 page too. A lambda to redirect non-index.html paths is in place: https://grrr.tech/posts/cloudfront-www-redirect/

> content of the `main.tf` file:

```
provider "aws" {
  region = var.AWS_REGION
}

module "common" {
  source       = "../../../../modules/common"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = var.ENVIRONMENT
  PROJECT      = var.PROJECT
}

resource "random_pet" "pet" {
}

module "s3" {
  source        = "../../../../modules/s3/bucket"
  globals       = module.common.globals
  BUCKET_NAME   = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-${random_pet.pet.id}-distribution-bucket"
  SSE_ALGORITHM = "AES256"
  FORCE_DESTROY = true
}

module "frontend" {
  source  = "../../../../modules/cloudfront"
  globals = module.common.globals
  NAME    = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-distribution"
  ELB_ORIGIN = [
    {
      ELB_DOMAIN_NAME = "cat-test.idemia.io"
      ELB_NAME        = "test-elb"
      custom_header = [{
            custom_header_name  = "foo"
            custom_header_value = "bar"
          }]
    }
  ]
}

resource "aws_s3_bucket_object" "index" {
  key                    = "index.html"
  bucket                 = module.bucket_1.s3_bucket_name
  source                 = "./index.html"
  etag                   = filemd5("./index.html")
  server_side_encryption = "AES256"
}
```

> content of the `variables.tf` file:

```
data "aws_caller_identity" "current" {}

variable "AWS_REGION" {
  default     = "us-east-1"
  description = "Provide aws region"
}

variable "ENVIRONMENT" {
  default     = "test"
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
}

variable "PROJECT" {
  description = "Provide name of the project (max 20 characters)"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}

variable "NAME" {
  description = "Name for Athena tables and DBs"
  default     = "database"
}
```

> content of the `terraform.tfvars` file:

```
##
# Provide aws region
##
AWS_REGION = "us-east-1"

##
# Provide name of the project (max 20 characters)
##
PROJECT = "cloudfront"

##
# Provide type of the environment, ex dev|test|stg|prod (max 4 characters)
##
ENVIRONMENT = "frontend"

##
# Provide aws account name
##
ACCOUNT_NAME = "teribu"
```
