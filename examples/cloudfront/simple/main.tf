provider "aws" {
  region = var.AWS_REGION
}

module "common" {
  source       = "git::https://bitbucket.org/digitallabsbuild/common.git//modules?ref=1.0.0"
  ACCOUNT_NAME = var.ACCOUNT_NAME
  AWS_REGION   = var.AWS_REGION
  ENVIRONMENT  = var.ENVIRONMENT
  PROJECT      = var.PROJECT
}

resource "random_pet" "pet" {
  length = 2
}

module "s3" {
  source        = "git::https://bitbucket.org/digitallabsbuild/s3.git//modules/bucket?ref=master"
  globals       = module.common.globals
  BUCKET_NAME   = "${var.ACCOUNT_NAME}-${var.ENVIRONMENT}-${var.PROJECT}-${random_pet.pet.id}-dist-bucket" # cannot be over 63 chars
  SSE_ALGORITHM = "AES256"
  FORCE_DESTROY = true
}

module "frontend" {
  source      = "../../../modules"
  globals     = module.common.globals
  BUCKET_NAME = module.s3.s3_bucket_name
  ORIGIN = [{
    bucket_name        = module.s3.s3_bucket_name
    bucket_domain_name = module.s3.bucket_domain_name
    custom_header = [{
      custom_header_name  = "foo"
      custom_header_value = "bar"
    }]
  }]
}

resource "aws_s3_bucket_object" "index" {
  key                    = "index.html"
  bucket                 = module.s3.s3_bucket_name
  source                 = "./index.html"
  etag                   = filemd5("./index.html")
  server_side_encryption = "AES256"
}
