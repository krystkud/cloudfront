output "CloudFront" {
  value = <<OUTPUT
(ง '̀͜ '́ )ง FRONTEND

ID           :: ${module.frontend.cf_id}

ARN          :: ${module.frontend.cf_arn}

Domain Name  :: ${module.frontend.cf_domain_name}

Bucket 1 Name  :: ${module.s3.s3_bucket_name}

Bucket 1 ARN   :: ${module.s3.s3_bucket_arn}

CHECK to see if all is working
curl https://${module.frontend.cf_domain_name}/index.html

OUTPUT
}
