variable "AWS_REGION" {
  default     = "us-east-1"
  description = "Provide aws region"
}

variable "ENVIRONMENT" {
  default     = "test"
  description = "Provide type of the environment, ex dev|test|stg|prod (max 4 characters)"
}

variable "PROJECT" {
  description = "Provide name of the project (max 20 characters)"
}

variable "ACCOUNT_NAME" {
  description = "Provide aws account name"
}
