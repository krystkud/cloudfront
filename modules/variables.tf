variable MODULE_VERSION {
  default     = "1.0.2"
  type        = string
  description = "Module version, update this value on commit to generate changelog and release"
}

##
# Define locale variables which will contains default tags
##
locals {
  default_tags = {
    "Environment" = var.globals["Environment"]
    "Project"     = var.globals["Project"]
    "AccountName" = var.globals["AccountName"]
    "CreatedBy"   = "terraform"
    "Region"      = var.globals["Region"]
    "Repository"  = lookup(var.globals, "Repository", null)
    "Builder"     = lookup(var.globals, "Builder", null)
    "Module"      = "TFM-cloudfront-module-${var.MODULE_VERSION}"
  }
  environment_name = "${var.globals["Project"]}-${var.globals["Environment"]}"
}

variable "globals" {
  description = "Provide global variables from common module"
  type        = map
}

variable "CERTIFICATE_ARN" {
  description = "Existing certificate arn. It must be in N.Virginia"
  type        = string
  default     = ""
}

variable "DOMAINS" {
  type        = list(string)
  default     = []
  description = "List of Domains to use in CloudFront"
}

variable "WEB_ACL_ID" {
  default     = ""
  description = "Provide global waf id"
  type        = string
}

variable "COMPRESS" {
  default     = false
  description = "Whether you want CloudFront to automatically compress content for web requests that include Accept-Encoding: gzip in the request header (default: false)."
  type        = bool
}

variable "IPV6_ENABLED" {
  default     = false
  description = "Whether the IPv6 is enabled for the distribution."
  type        = bool
}

variable "DEFAULT_ROOT_OBJECT" {
  default     = ""
  description = "The object that you want CloudFront to return (for example, index.html) when an end user requests the root URL."
  type        = string
}

variable "MINIMUM_PROTOCOL_VERSION" {
  type        = string
  default     = "TLSv1.2_2021" 
  description = "The minimum version of the SSL protocol that you want CloudFront to use for HTTPS connections. One of SSLv3, TLSv1, TLSv1_2016, TLSv1.1_2016 or TLSv1.2_2018. Default: TLSv1.2_2018. NOTE: If you are using a custom certificate (specified with acm_certificate_arn or iam_certificate_id), and have specified sni-only in ssl_support_method, TLSv1 or later must be specified. If you have specified vip in ssl_support_method, only SSLv3 or TLSv1 can be specified. If you have specified cloudfront_default_certificate, TLSv1 must be specified"
}

variable "SSL_SUPPORT_METHOD" {
  default     = "sni-only"
  description = "Specifies how you want CloudFront to serve HTTPS requests. One of vip or sni-only. Required if you specify acm_certificate_arn or iam_certificate_id. NOTE: vip causes CloudFront to use a dedicated IP address and may incur extra charges"
  type        = string
}

variable "PRICE_CLASS" {
  default     = "PriceClass_100"
  type        = string
  description = "The price class for this distribution. One of PriceClass_All, PriceClass_200, PriceClass_100"
}

variable "VIEWER_PROTOCOL_POLICY" {
  default     = "redirect-to-https"
  description = "Use this element to specify the protocol that users can use to access the files in the origin specified by TargetOriginId when a request matches the path pattern in PathPattern. One of allow-all, https-only, or redirect-to-https"
  type        = string
}

variable "ALLOWED_METHODS" {
  type        = list(string)
  default     = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
  description = "(Required) - Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket or your custom origin."
}

variable "CACHED_METHODS" {
  type        = list(string)
  description = "Controls whether CloudFront caches the response to requests using the specified HTTP methods."
  default     = ["GET", "HEAD"]
}

variable "MIN_TTL" {
  default     = 0
  type        = number
  description = "The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated. Defaults to 0 seconds."
}

variable "MAX_TTL" {
  default     = 31536000
  type        = number
  description = "The maximum amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated. Only effective in the presence of Cache-Control max-age, Cache-Control s-maxage, and Expires headers. Defaults to 365 days."
}

variable "DEFAULT_TTL" {
  default     = 60
  type        = number
  description = "The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of an Cache-Control max-age or Expires header. Defaults to 1 day."
}

variable "BUCKET_NAME" {
  default     = ""
  description = "S3 bucket name"
  type        = string
}

variable "ORIGIN_PROTOCOL_POLICY" {
  default     = "http-only"
  description = "The origin protocol policy to apply to your origin. One of http-only, https-only, or match-viewer"
  type        = string
}

variable "ORIGIN_READ_TIMEOUT" {
  default     = 60
  description = "The Custom Read timeout, in seconds. By default, AWS enforces a limit of 60"
  type        = number
}

variable "ORIGIN_SSL_PROTOCOLS" {
  default     = ["TLSv1"]
  description = "The SSL/TLS protocols that you want CloudFront to use when communicating with your origin over HTTPS. A list of one or more of SSLv3, TLSv1, TLSv1.1, and TLSv1.2"
  type        = list(string)
}

variable "ACCESS_LOGS_BUCKET" {
  default     = ""
  type        = string
  description = "Use with additional resource modules/s3/logs S3 LOGS_BUCKET to enable S3 logs for CloudFront"
}

variable "INCLUDE_COOKIES" {
  default     = true
  description = "Specifies whether you want CloudFront to include cookies in access logs"
  type        = bool
}

variable "ACCESS_LOGS_BUCKET_PREFIX" {
  description = "An optional string that you want CloudFront to prefix to the access log filenames for this distribution, for example, myprefix/"
  default     = ""
  type        = string
}

variable "ORDERED_CACHE_BEHAVIOR" {
  type        = list
  description = "An ordered list of cache behaviors resource for this distribution. List from top to bottom in order of precedence. The topmost cache behavior will have precedence 0. The arguments for default_cache_behavior are the same as for ordered_cache_behavior, except for the path_pattern argument is not required."
  default     = []
}

variable "ORIGIN" {
  description = "Location where content is stored, and from which CloudFront gets content to serve to viewers."
  type        = list(any)
  default     = []
}

variable "TRUSTED_SIGNERS" {
  type        = list(string)
  default     = []
  description = "The AWS accounts, if any, that you want to allow to create signed URLs for private content. 'self' is acceptable."
}

variable "FORWARDED_COOKIES" {
  type        = string
  default     = "none"
  description = "Specifies whether you want CloudFront to forward cookies to the origin that is associated with this cache behavior. You can specify all, none or whitelist. If whitelist, you must include the subsequent whitelisted_names"
}

variable "FORWARDED_COOKIES_ORDERED" {
  type        = string
  default     = "none"
  description = "Specifies whether you want CloudFront to forward cookies to the origin that is associated with this cache behavior. You can specify all, none or whitelist. If whitelist, you must include the subsequent whitelisted_names"
}

variable "FORWADED_QUERY_STRING" {
  type        = bool
  default     = false
  description = "Indicates whether you want CloudFront to forward query strings to the origin that is associated with this cache behavior."
}

variable "FORWADED_QUERY_STRING_ORDERED" {
  type        = bool
  default     = false
  description = "Indicates whether you want CloudFront to forward query strings to the origin that is associated with this cache behavior."
}

variable "FORWARDED_HEADERS" {
  type        = list
  default     = []
  description = "Specifies the Headers, if any, that you want CloudFront to vary upon for this cache behavior. Specify * to include all headers."
}

variable "FORWARDED_HEADERS_ORDERED" {
  type        = list
  default     = []
  description = "Specifies the Headers, if any, that you want CloudFront to vary upon for this cache behavior. Specify * to include all headers."
}

variable "CUSTOM_ERROR_RESPONSE" {
  description = "One or more custom error response elements (multiples allowed)."
  type        = list(map(any))
  default     = []
}

variable "ELB_ORIGIN" {
  description = "ELB Origin config.Example  {ELB_ZONE_ID = module.elb.zone_id and ELB_DOMAIN_NAME = module.elb.dns_name}"
  type        = list(any)
  default     = []
}

variable "ELB_ZONE_ID" {
  default     = ""
  description = "The AWS hosted zone ID of the load balancer (output zone_id of ELB modules)"
  type        = string
}

variable "DEFAULT_LAMBDA_FUNCTION_ASSOCIATION" {
  description = "List of Lambdas ARN for Function Associations, must be in N.Virginia"
  default     = []
  type        = list(any)
}
