resource "aws_s3_bucket_policy" "b" {
  count  = length(var.ORIGIN)
  bucket = lookup(var.ORIGIN[count.index], "bucket_name", null)
  policy = data.aws_iam_policy_document.s3_policy_cf_bucket[count.index].json
}

data "aws_iam_policy_document" "s3_policy_cf_bucket" {
  count = length(var.ORIGIN)
  statement {
    actions   = ["s3:GetObject"]
    resources = ["arn:aws:s3:::${lookup(var.ORIGIN[count.index], "bucket_name", null)}/*"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity[count.index].iam_arn]
    }
  }

  statement {
    actions   = ["s3:ListBucket"]
    resources = ["arn:aws:s3:::${lookup(var.ORIGIN[count.index], "bucket_name", null)}"]

    principals {
      type        = "AWS"
      identifiers = [aws_cloudfront_origin_access_identity.origin_access_identity[count.index].iam_arn]
    }
  }
}

# --- CloudFront ---
resource "aws_cloudfront_origin_access_identity" "origin_access_identity" {
  count   = length(var.ORIGIN) > 0 ? length(var.ORIGIN) : length(var.ELB_ORIGIN)
  comment = "access-identity-${length(var.ORIGIN) == 0 ? lookup(var.ELB_ORIGIN[count.index], "ELB_ZONE_ID", null) : lookup(var.ORIGIN[count.index], "bucket_name", null)}"
}

#tfsec:ignore:aws-cloudfront-enable-logging
resource "aws_cloudfront_distribution" "cf" {
  enabled             = true
  is_ipv6_enabled     = var.IPV6_ENABLED
  default_root_object = var.DEFAULT_ROOT_OBJECT
  price_class         = var.PRICE_CLASS
  aliases             = var.DOMAINS
  web_acl_id          = var.WEB_ACL_ID == "" ? null : var.WEB_ACL_ID

  dynamic "custom_error_response" {
    for_each = var.CUSTOM_ERROR_RESPONSE
    content {
      error_caching_min_ttl = lookup(custom_error_response.value, "error_caching_min_ttl", null)
      error_code            = lookup(custom_error_response.value, "error_code", null)
      response_code         = lookup(custom_error_response.value, "response_code", null)
      response_page_path    = lookup(custom_error_response.value, "response_page_path", null)
    }
  }

  dynamic "logging_config" { 
    for_each = var.ACCESS_LOGS_BUCKET == "" ? [] : list(var.ACCESS_LOGS_BUCKET)
    content {
      include_cookies = var.INCLUDE_COOKIES
      bucket          = var.ACCESS_LOGS_BUCKET
      prefix          = var.ACCESS_LOGS_BUCKET_PREFIX
    }
  }

  dynamic "origin" {
    for_each = var.ORIGIN
    content {
      domain_name = lookup(origin.value, "bucket_domain_name", null)
      origin_id   = lookup(origin.value, "bucket_name", null)
      dynamic "custom_header" {
        for_each = lookup(origin.value, "custom_header", [])
        content {
          name  = lookup(custom_header.value, "custom_header_name", null)
          value = lookup(custom_header.value, "custom_header_value", null)
        }
      }

      s3_origin_config {
        origin_access_identity = aws_cloudfront_origin_access_identity.origin_access_identity[lookup(origin.value, "s3_origin_config", 0)].cloudfront_access_identity_path
      }
    }
  }

  dynamic "origin" {
    for_each = var.ELB_ORIGIN
    content {
      domain_name = lookup(origin.value, "ELB_DOMAIN_NAME")
      origin_id   = lookup(origin.value, "ELB_ZONE_ID")
      dynamic "custom_header" {
        for_each = lookup(origin.value, "custom_header", [])
        content {
          name  = lookup(custom_header.value, "custom_header_name", null)
          value = lookup(custom_header.value, "custom_header_value", null)
        }
      }

      custom_origin_config {
        http_port              = 80
        https_port             = 443
        origin_protocol_policy = var.ORIGIN_PROTOCOL_POLICY
        origin_ssl_protocols   = var.ORIGIN_SSL_PROTOCOLS
        origin_read_timeout    = var.ORIGIN_READ_TIMEOUT
      }
    }
  }

  viewer_certificate {
    cloudfront_default_certificate = var.CERTIFICATE_ARN == "" ? true : false
    acm_certificate_arn            = var.CERTIFICATE_ARN == "" ? null : var.CERTIFICATE_ARN
    ssl_support_method             = var.SSL_SUPPORT_METHOD
    minimum_protocol_version       = var.MINIMUM_PROTOCOL_VERSION #tfsec:ignore:aws-cloudfront-use-secure-tls-policy
  }

  default_cache_behavior {
    allowed_methods        = var.ALLOWED_METHODS
    cached_methods         = var.CACHED_METHODS
    target_origin_id       = length(var.ORIGIN) == 0 ? lookup(var.ELB_ORIGIN[0], "ELB_ZONE_ID") : lookup(var.ORIGIN[0], "bucket_name", null)
    compress               = var.COMPRESS
    viewer_protocol_policy = var.VIEWER_PROTOCOL_POLICY
    min_ttl                = var.MIN_TTL
    default_ttl            = var.DEFAULT_TTL
    max_ttl                = var.MAX_TTL
    trusted_signers        = var.TRUSTED_SIGNERS

    dynamic "lambda_function_association" {
      for_each = var.DEFAULT_LAMBDA_FUNCTION_ASSOCIATION
      content {
        event_type   = lookup(lambda_function_association.value, "event_type", "origin-request")
        include_body = false
        lambda_arn   = lookup(lambda_function_association.value, "lambda_arn", null)
      }
    }

    forwarded_values {
      query_string = var.FORWADED_QUERY_STRING
      headers      = var.FORWARDED_HEADERS

      cookies {
        forward = var.FORWARDED_COOKIES
      }
    }
  }

  dynamic "ordered_cache_behavior" {
    for_each = var.ORDERED_CACHE_BEHAVIOR
    content {
      path_pattern           = lookup(ordered_cache_behavior.value, "path_pattern", null)
      allowed_methods        = lookup(ordered_cache_behavior.value, "allowed_methods", var.ALLOWED_METHODS)
      cached_methods         = lookup(ordered_cache_behavior.value, "cached_methods", var.CACHED_METHODS)
      target_origin_id       = lookup(ordered_cache_behavior.value, "target_origin_id", var.BUCKET_NAME == "" ? var.ELB_ZONE_ID : var.BUCKET_NAME)
      compress               = lookup(ordered_cache_behavior.value, "compress", var.COMPRESS)
      viewer_protocol_policy = lookup(ordered_cache_behavior.value, "viewer_protocol_policy", var.VIEWER_PROTOCOL_POLICY)
      min_ttl                = lookup(ordered_cache_behavior.value, "min_ttl", var.MIN_TTL)
      default_ttl            = lookup(ordered_cache_behavior.value, "default_ttl", var.DEFAULT_TTL)
      max_ttl                = lookup(ordered_cache_behavior.value, "max_ttl", var.MAX_TTL)
      trusted_signers        = lookup(ordered_cache_behavior.value, "trusted_signers", var.TRUSTED_SIGNERS)

      dynamic "lambda_function_association" {
        for_each = lookup(ordered_cache_behavior.value, "lambda_function_association", [])
        content {
          event_type   = lookup(lambda_function_association.value, "event_type", "origin-request")
          include_body = false
          lambda_arn   = lookup(lambda_function_association.value, "lambda_arn", null)
        }
      }

      forwarded_values {
        query_string = var.FORWADED_QUERY_STRING_ORDERED
        headers      = var.FORWARDED_HEADERS_ORDERED

        cookies {
          forward = var.FORWARDED_COOKIES_ORDERED
        }
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  tags = merge(
    local.default_tags,
    map(
      "Name", "${local.environment_name}-cf",
      "Object", "aws_cloudfront_distribution"
    )
  )
}
