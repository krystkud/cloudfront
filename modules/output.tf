output "cf_id" {
  description = "The identifier for the distribution. For example: EDFDVBD632BHDS5."
  value       = aws_cloudfront_distribution.cf.id
}

output "cf_arn" {
  value       = aws_cloudfront_distribution.cf.arn
  description = "The ARN (Amazon Resource Name) for the distribution. For example: arn:aws:cloudfront::123456789012:distribution/EDFDVBD632BHDS5, where 123456789012 is your AWS account ID."
}

output "cf_status" {
  value       = aws_cloudfront_distribution.cf.status
  description = "The current status of the distribution. Deployed if the distribution's information is fully propagated throughout the Amazon CloudFront system."
}

output "cf_trusted_signers" {
  value       = aws_cloudfront_distribution.cf.trusted_signers
  description = "trusted_signers - List of nested attributes for active trusted signers, if the distribution is set up to serve private content with signed URLs."
}

output "cf_domain_name" {
  description = "The domain name corresponding to the distribution. For example: d604721fxaaqy9.cloudfront.net."
  value       = aws_cloudfront_distribution.cf.domain_name
}

output "cf_etag" {
  description = "The current version of the distribution's information. For example: E2QWRUHAPOMQZL."
  value       = aws_cloudfront_distribution.cf.etag
}

output "cf_hosted_zone_id" {
  description = "The CloudFront Route 53 zone ID that can be used to route an Alias Resource Record Set to. This attribute is simply an alias for the zone ID Z2FDTNDATAQYW2."
  value       = aws_cloudfront_distribution.cf.hosted_zone_id
}
