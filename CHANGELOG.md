# Change Log
-------------
## 1.0.3 2021-08-19
TM-467: Added Jenkinsfile.

## 1.0.2 2021-04-20
TM-400: AWS provider unlocked.

## 1.0.1 2020-12-17
TM-258: Allowed configuration of S3 and ELB origins together.

## 1.0.0 2020-09-15
TM-126: Release module.
TM-122: Rename ELB_NAME to ELB_ZONE_ID.

## 0.1.1 2020-08-14
Tm-88: Added tests.
TM-94: Cloudfront module modifications.
TM-92: Fix aws provider.

## 0.1.0 2020-07-22
TM-8: Initial commit.

#### Added
- Initial package for CloudFront
