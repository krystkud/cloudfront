*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/cloudfront/simple
${region}        eu-central-1

*** Test Cases ***
Test simple
    Terraform init    ${example_path}
    Terraform apply   ${example_path}
    ${terraform_response}=   Terraform get json module values  ${example_path}  frontend  aws_cloudfront_distribution.cf
    Set suite variable  ${frontend_id}  ${terraform_response['values']['id']}
    Set suite variable  ${frontend_domain_name}  ${terraform_response['values']['domain_name']}

    ${terraform_resp}=  Terraform get json module values  ${example_path}  s3  aws_s3_bucket.s3_bucket
    Set suite variable  ${s3_name}  ${terraform_resp['values']['bucket']}

    AWSession.spawn  cloudfront  ${region}
    ${aws_api_resp}=  AWSession.get  list_distributions
    ${aws_frontend_id_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Id  ${frontend_id}
    Should not be empty  ${aws_frontend_id_json}

    ${aws_domain_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  DomainName  ${frontend_domain_name}
    Should not be empty  ${aws_domain_name_json}

    AWSession.spawn  s3  ${region}
    ${aws_api_resp}=  AWSession.get  list_buckets
    ${aws_s3_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${s3_name}
    Should not be empty  ${aws_s3_name_json}


Test simple-destroy
    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}
    AWSession.spawn  cloudfront  ${region}
    ${aws_api_resp}=  AWSession.get  list_distributions
    ${aws_frontend_id_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Id  ${frontend_id}
    Should be empty  ${aws_frontend_id_json}

    AWSession.spawn  s3  ${region}
    ${aws_api_resp}=  AWSession.get  list_buckets
    ${aws_s3_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${s3_name}
    Should be empty  ${aws_s3_name_json}