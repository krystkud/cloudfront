*** Settings ***
Documentation  Test suite dedicated to test examples provided by module.
...            This suite utilizes examples, deploys them one by one and verify it's creation via AWS API
...            Verification is also done for complete removal of the example module
...            Example module provides AWS MVP - Minimal Valuable Product, thus it should not be
...            treated as a sustainable source for environment
Metadata       Version     0.1.0

Resource  resources/terraform.robot
Resource  resources/module_tests.robot

*** Variables ***
${PROJECT_NAME}
${example_path}  ${EXECDIR}/examples/cloudfront/complex
${region}        eu-central-1

*** Test Cases ***
Test complex
    [Documentation]  Temporary disabled due to problem related with destroing lambdas on AWS.
    Pass Execution  Temporary disabled
#    ${random_env}=  Generate Random String  8  [LOWER]
#    Set suite variable  ${env}  ${random_env}
#    Terraform init  ${example_path}
#    Terraform apply  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.frontend_logs_bucket
#    Terraform apply  ${example_path}  var={'ENVIRONMENT':'${env}'}
#    ${terraform_response}=   Terraform get json module values  ${example_path}  frontend  aws_cloudfront_distribution.cf
#    Set suite variable  ${frontend_id}  ${terraform_response['values']['id']}
#    Set suite variable  ${frontend_domain_name}  ${terraform_response['values']['domain_name']}
#
#    ${terraform_response}=   Terraform get json module values  ${example_path}  athena  aws_athena_database.main
#    Set suite variable  ${athena_name}  ${terraform_response['values']['name']}
#
#    ${terraform_output_resp}=  Terraform get json output value with name  ${example_path}  queries
#    Set suite variable  ${create_cloudfront_logs}  ${terraform_output_resp['create_cloudfront_logs']}
#    Set suite variable  ${create_s3_access_logs}  ${terraform_output_resp['create_s3_access_logs']}
#    Set suite variable  ${create_waf_logs}  ${terraform_output_resp['create_waf_logs']}
#
#    ${terraform_resp}=  Terraform get json module values  ${example_path}  bucket_1  aws_s3_bucket.s3_bucket
#    Set suite variable  ${bucket_1_name}  ${terraform_resp['values']['bucket']}
#
#    ${terraform_resp}=  Terraform get json module values  ${example_path}  bucket_2  aws_s3_bucket.s3_bucket
#    Set suite variable  ${bucket_2_name}  ${terraform_resp['values']['bucket']}
#
#    ${terraform_resp}=  Terraform get json module values  ${example_path}  bucket_3  aws_s3_bucket.s3_bucket
#    Set suite variable  ${bucket_3_name}  ${terraform_resp['values']['bucket']}
#
#    ${terraform_resp}=  Terraform get json module values  ${example_path}  frontend_logs_bucket  aws_s3_bucket.s3_logs
#    Set suite variable  ${frontend_logs_bucket_name}  ${terraform_resp['values']['bucket']}
#
#    AWSession.spawn  cloudfront  ${region}
#    ${aws_api_resp}=  AWSession.get  list_distributions
#    ${aws_frontend_id_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Id  ${frontend_id}
#    Should not be empty  ${aws_frontend_id_json}
#
#    ${aws_domain_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  DomainName  ${frontend_domain_name}
#    Should not be empty  ${aws_domain_name_json}
#
#    AWSession.spawn  athena  ${region}
#    ${aws_api_resp}=  AWSession.get  list_databases  CatalogName=AwsDataCatalog
#    ${aws_athena_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${athena_name}
#    Should not be empty  ${aws_athena_name_json}
#
#    ${aws_api_resp}=  AWSession.get  list_named_queries  WorkGroup=${athena_name}
#    ${create_alb_logs_json}=  AWSession.filter_list  ${aws_api_resp}  ${create_cloudfront_logs}
#    Should not be empty  ${create_alb_logs_json}
#
#    ${create_cloudfront_logs_json}=  AWSession.filter_list  ${aws_api_resp}  ${create_s3_access_logs}
#    Should not be empty  ${create_cloudfront_logs_json}
#
#    ${create_nlb_logs_json}=  AWSession.filter_list  ${aws_api_resp}  ${create_waf_logs}
#    Should not be empty  ${create_nlb_logs_json}
#
#    AWSession.spawn  s3  ${region}
#    ${aws_api_resp}=  AWSession.get  list_buckets
#    ${aws_bucket_1_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${bucket_1_name}
#    Should not be empty  ${aws_bucket_1_name_json}
#
#    ${aws_api_resp}=  AWSession.get  list_buckets
#    ${aws_bucket_2_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${bucket_2_name}
#    Should not be empty  ${aws_bucket_2_name_json}
#
#    ${aws_api_resp}=  AWSession.get  list_buckets
#    ${aws_bucket_3_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${bucket_3_name}
#    Should not be empty  ${aws_bucket_3_name_json}
#
#    ${aws_api_resp}=  AWSession.get  list_buckets
#    ${aws_frontend_logs_bucket_name_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Name  ${frontend_logs_bucket_name}
#    Should not be empty  ${aws_frontend_logs_bucket_name_json}
#
#    ${result}=  Run process  curl https://website.cat-test.idemia.io  shell=yes
#    Log many  ${result.stdout}  ${result.stderr}
#    Should contain  ${result.stdout}  CAT ERROR
#
#    ${result}=  Run process  curl https://website.cat-test.idemia.io/front2/index.html  shell=yes
#    Log many  ${result.stdout}  ${result.stderr}
#    Should contain  ${result.stdout}  CAT ERROR
#
#    ${result}=  Run process  curl https://website.cat-test.idemia.io/front3/index.html  shell=yes
#    Log many  ${result.stdout}  ${result.stderr}
#    Should contain  ${result.stdout}  CAT ERROR
#
#Test complex-destroy
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.frontend
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=aws_route53_record.cloudfront_domain_name
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.wildcard_cert
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.common
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.athena
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.athena_bucket
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.waf_global
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.frontend_logs_bucket
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.bucket_1
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.bucket_2
#    Wait Until Keyword Succeeds  20 min  5 sec   Terraform destroy  ${example_path}  var={'ENVIRONMENT':'${env}'}  target=module.bucket_3
#    AWSession.spawn  cloudfront  ${region}  var={'ENV':${env}}
#    ${aws_api_resp}=  AWSession.get  list_distributions
#    ${aws_frontend_id_json}=  AWSession.filter_by_key_and_value  ${aws_api_resp}  Id  ${frontend_id}
#    Should be empty  ${aws_frontend_id_json}
